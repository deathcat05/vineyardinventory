# README #

This is a vineyard database currently being utilized by both customers and team members. It is used by customers for them to know where Valley Vineyards came from and to see the wines that they have to offer. Everything else is mainly utilized by the team members. 



##### NOTES ABOUT PROJECT ######

The following things are implemented in my database, but not outputting to the webpage: 

(1) A view (that uses a GROUP BY and AGGREGATE function) to display users who have bought more than three bottles of white wine. 

(2) The BoughtRed table is not properly implemented, and the BoughtWhite table is not implemented at all. However, it does not take away from the current functionality of the webpage.